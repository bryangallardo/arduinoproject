#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DS3231_Simple.h>
#include <Adafruit_NeoPixel.h>

// Pin Locations
#define D13PIN 13
#define REDPIN 2
#define GREENPIN 3

// Flag Locations for Bit Flags
#define D13BLINKING 6
#define BICOLORBLINKING 5
#define RGBBLINKING 4
#define D13ON 3
#define BICOLORON 2
#define RGBON 1

#define t_LED 1
#define t_RED 2
#define t_GREEN 3
#define t_D13 13
#define t_SET 4
#define t_ON 5
#define t_OFF 6
#define t_BLINK 7
#define t_STATUS 8
#define t_VERSION 9
#define t_HELP 10
#define t_LEDS 11
#define t_TIME 12
#define t_READ 13
#define t_WRITE 14
#define t_DHT 15
#define t_PRINT 16
#define t_ADD 17
#define t_EXTREME 18
#define t_RGB 19
#define t_EOL 255
#define t_WORD 254
#define t_DHTPIN 7
#define t_RGBPIN 6
#define t_RGBCLOCKPIN 5
#define NUM_LEDS 1
#define DHTTYPE DHT22

#define INPUT_BUFFER_SIZE 255
#define TOKEN_BUFFER_SIZE 6
#define COMMAND_TABLE_SIZE 20

// Struct for logging temp and humidity
struct Log{
   byte humidity;   // 1 byte
   float temp;      // 4 bytes
};

const char versionNumber[] = "0.0.5";
unsigned int delayInterval = 500;
unsigned int newInterval = 0;

unsigned char ch;

DateTime MyDateTime;
Adafruit_NeoPixel leds(NUM_LEDS, t_RGBPIN, NEO_GRB + NEO_KHZ800);

float highestTemp = 0;
float lowestTemp = 0;
float temp = 0;
byte humidity = 0;


/*
 * New Data
 *
 */

//                         
//                     DBR : Flag for LED D13, BiColor, and RGB (respesctively) as ON
//                  DBR||| : Flag for LEDs D13, BiColor, and RGB as BLINKING ON
//                --|||||| : Two MSBs are unused
byte ledFlags = 0b00000000;


// Default Blinking Patterns for each LED
byte d13LedPattern = 0b11111111;
byte biColorLedPattern = 0b10101010; // Even bits = RED, Odd bits = GREEN, Stride = 2
byte RGBLedPattern = 0b10101010;

// Blinking delay interval
unsigned int d13LedDelayInterval = 500;
unsigned int biColorLedDelayInterval = 500;
unsigned int RGBLedDelayInterval = 500;

// Next time when LED will blink
unsigned long d13LedActionTime;
unsigned long biColorLedActionTime;
unsigned long RGBLedActionTime;

// RGB LED Colors
unsigned char r = 0,  g = 0, b = 0;

/*
 *
 */

unsigned int inputBufferIndex = 0;
unsigned int tokenBufferIndex = 0;
unsigned char inputBuffer[INPUT_BUFFER_SIZE];
unsigned char tokenBuffer[TOKEN_BUFFER_SIZE];
unsigned char commandTable[COMMAND_TABLE_SIZE][4] = { {'l', 'e', 3, t_LED}, 
                           {'d', '1', 3, t_D13},                           {'g', 'r', 5, t_GREEN},
                           {'r', 'e', 3, t_RED},
                           {'o', 'n', 2, t_ON},
                           {'o', 'f', 3, t_OFF},
                           {'l', 'e', 4, t_LEDS},
                           {'b', 'l', 5, t_BLINK},
                           {'s', 'e', 3, t_SET},
                           {'s', 't', 6, t_STATUS},
                           {'v', 'e', 7, t_VERSION},
                           {'h', 'e', 4, t_HELP},
                           {'t', 'i', 4, t_TIME},
                           {'r', 'e', 4, t_READ},
                           {'w', 'r', 5, t_WRITE},
                           {'d', 'h', 3, t_DHT},
                           {'p', 'r', 5, t_PRINT},
                           {'a', 'd', 3, t_ADD},
                           {'e', 'x', 7, t_EXTREME},
                           {'r', 'g', 3, t_RGB} };

// DS3231 Clock
DS3231_Simple Clock;

DHT dht22(t_DHTPIN, DHTTYPE);

/***********************
 * Function Prototypes *
 ***********************/
void multitasker(void);

//Helper functions
bool readBitFromByte(byte byteField, int bit);
float convertToF(float cel);
byte leftRotate(byte bitfield, unsigned int n);
void getInput(void);
bool isANumber(int bufferSize);
unsigned int parseInputToUnsignedNumber(unsigned int min_size, unsigned int max_size);
//
// Functions
//


// Dump log
void dumpLog()
{
   Log loggedData;
   DateTime loggedTime;

   unsigned int count = 0;
   while(Clock.readLog(loggedTime, loggedData))
   {
      if(count  == 0)
      {
         Serial.println();
         Serial.println(F("Date   | Temperature   | Humidity"));
      }

      count++;
      Clock.printTo(Serial, loggedTime);
      Serial.print(F("  "));
      Serial.print(loggedData.temp);
      Serial.print(F("C  "));
      Serial.println(loggedData.humidity);
   }
   Serial.println();
   Serial.print(F("# of Log Entries Found: "));
   Serial.println(count);

}

// Turns the character ascii value into an integer value
unsigned char charToInt(char c)
{
   if (c >= '0' && c <= '9')
      return c - '0';
   else
      return 0;
}

unsigned int power(int x, unsigned int y)
{
   unsigned int result = 1;

   for(unsigned int i = 0; i < y; i++)
         result *= x;

   return result;
}

void executeTokens(void)
{
   switch (tokenBuffer[0])
   {
      case t_EOL:
         break;
      case t_D13:
      {
         switch (tokenBuffer[1])
         {
            case t_ON:
               ledFlags = ledFlags | 0b00000100;
               break;
            case t_OFF:
               ledFlags = ledFlags & 0b11111011;
               break;
            case t_BLINK:
               if (readBitFromByte(ledFlags, D13BLINKING))
               {
                  d13LedPattern = 0b11111111;
                  ledFlags = ledFlags & 0b11011111;
               }     
               else
               {
                  d13LedPattern = 0b10101010;
                  ledFlags = ledFlags | 0b00100000;
               }
               break;
            default:
               Serial.println(F("ERROR: Invalid command syntax. D13 < ON | OFF | BLINK>"));
         }
         break;
      }

      case t_LED:
      {
         switch (tokenBuffer[1])
         {
            case t_ON:
               ledFlags = ledFlags | 0b00000010;
               break;
            case t_OFF:
               ledFlags = ledFlags & 0b11111101;
               break;
            case t_GREEN:
            case t_RED:
               biColorLedPattern = biColorLedPattern ^ 0b11111111;
               break;
            case t_BLINK:
               // Light is NOT Blinking
               if ( (readBitFromByte(ledFlags, BICOLORBLINKING) == 0))
               {
                  // Light is NOT Blinking and RED
                  if (readBitFromByte(biColorLedPattern, 8) == 1)
                     biColorLedPattern = 0b10001000;
                  // Light is NOT Blinking and GREEN
                  else
                     biColorLedPattern = 0b01000100;
                  
                  // Turn on Blinking Flag
                  ledFlags = ledFlags | 0b00000010;
               }
               // Light is BLINKING
               else
               {
                  // Light is BLINKING and RED
                  if ( (readBitFromByte(biColorLedPattern, 8) == 1) || (readBitFromByte(biColorLedPattern, 6) == 1) )
                  biColorLedPattern = 0b10101010;
                  // Light is BLINKING and GREEN
                  else
                     biColorLedPattern = 0b01010101;

                  // Turn off Blinking Flag
                  ledFlags = ledFlags & 0b11111101;
               }
               break;
            default:
               Serial.println(F("ERROR: Invalid command syntax. LED <GREEN | RED | OFF | BLINK>"));
         }
         break;
      }

      case t_SET:
      {
         switch(tokenBuffer[1])
         {
            case t_BLINK:
            {
               switch(tokenBuffer[2])
               {
                  case t_D13:
                     d13LedDelayInterval = newInterval;                  
                     newInterval = 0;
                     break;
                  case t_LED:
                     biColorLedDelayInterval = newInterval;
                     newInterval = 0;
                     break;
                  case t_RGB:
                     RGBLedDelayInterval = newInterval;
                     newInterval = 0;
                     break;
                  default:
                     Serial.println(F("ERROR: Invalid command syntax."));
               }
               break;
            }
            default:
               Serial.println(F("ERROR: Invalid command syntax. SET <D13 | LED | RGB > BLINK <0 - 65535>"));
         }
         break;
      }

      case t_STATUS:
      {
         switch(tokenBuffer[1])
         {
            case t_LEDS:
               Serial.print( F("D13 is set to "));
               Serial.print( readBitFromByte(ledFlags, D13ON) ? F("ON") : F("OFF"));
               Serial.print(F(" and BLINK is ")); 
               Serial.print(readBitFromByte(ledFlags, D13BLINKING) ? "ENABLED." : "DISABLED.");
               Serial.print(F("BLINK RATE is set to "));
               Serial.print(d13LedDelayInterval);
               Serial.println(" (ms).");

                
               Serial.print(F("BiColor LED is set to "));
               Serial.print( readBitFromByte(ledFlags, BICOLORON) ?  "ON" : "OFF");
               Serial.print(F(" and BLINK is "));
               Serial.println( (readBitFromByte(ledFlags, BICOLORBLINKING)? "ENABLED." : "DISABLED.") );
               Serial.print(F("BLINK RATE is set to "));
               Serial.print(biColorLedDelayInterval);
               Serial.println(" (ms).");

               Serial.print(F("RGB LED is set to "));
               Serial.print( readBitFromByte(ledFlags, RGBON) ?  "ON" : "OFF");
               Serial.print(F(" and BLINK is "));
               Serial.println( (readBitFromByte(ledFlags, RGBBLINKING)? "ENABLED." : "DISABLED.") );
               Serial.print(F("BLINK RATE is set to "));
               Serial.print(RGBLedDelayInterval);
               Serial.println(" (ms).");

                break;
            default:
                Serial.println(F("ERROR: Invalid command syntax. STATUS <LEDS>"));

         }
         break;
      }

      case t_TIME:
      {
         switch(tokenBuffer[1])
         {
            case t_READ:
               Serial.print(F("The current time is: "));
               Clock.printTimeTo_HMS(Serial);
               break;
            case t_WRITE:
               MyDateTime = Clock.read();
               Serial.println(F("Set the RTC's new time. \"t\" to increment. \"r\" to decrement."));
               Serial.print(F("Hour: "));
               Serial.println(MyDateTime.Hour);
               ch = Serial.read();
               
               while (ch != 13)
               {
                  if (ch == 13)
                     break;
                  if (ch == 't')
                  {
                     if (MyDateTime.Hour == 23)
                        MyDateTime.Hour = 0;
                     else
                        MyDateTime.Hour += 1; 

                     Serial.print(F("Hour: "));
                     Serial.println(MyDateTime.Hour);
                  }
                  if (ch == 'r')
                  {
                     if (MyDateTime.Hour == 0)
                        MyDateTime.Hour = 23;
                     else
                        MyDateTime.Hour -= 1;

                     Serial.print(F("Hour: "));
                     Serial.println(MyDateTime.Hour);
                  }
                  ch= Serial.read();
               }

               Serial.print(F("Minute: "));
               Serial.println(MyDateTime.Minute);
               ch = Serial.read();
               
               while (ch != 13)
               {
                  if (ch == 13)
                     break;
                  if (ch == 't')
                  {
                     if (MyDateTime.Minute == 59)
                        MyDateTime.Minute = 0;
                     else
                        MyDateTime.Minute += 1; 

                     Serial.print(F("Minute: "));
                     Serial.println(MyDateTime.Minute);
                  }
                  if (ch == 'r')
                  {
                     if (MyDateTime.Minute == 0)
                        MyDateTime.Minute = 59;
                     else
                        MyDateTime.Minute -= 1;

                     Serial.print(F("Minute: "));
                     Serial.println(MyDateTime.Minute);
                  }
                  ch= Serial.read();
               }

               Serial.print(F("Second: "));
               Serial.println(MyDateTime.Second);
               ch = Serial.read();
               
               while (ch != 13)
               {
                  if (ch == 13)
                     break;
                  if (ch == 't')
                  {
                     if (MyDateTime.Second == 59)
                        MyDateTime.Second = 0;
                     else
                        MyDateTime.Second += 1; 

                     Serial.print(F("Second: "));
                     Serial.println(MyDateTime.Second);
                  }
                  if (ch == 'r')
                  {
                     if (MyDateTime.Second == 0)
                        MyDateTime.Second = 59;
                     else
                        MyDateTime.Second -= 1;

                     Serial.print(F("Second: "));
                     Serial.println(MyDateTime.Second);
                  }
                  ch= Serial.read();
               }

               Clock.write(MyDateTime);
               Serial.print(F("The time has been set to: "));
               Clock.printTimeTo_HMS(Serial);
               break;
            default:
               Serial.println(F("ERROR: Invalid command syntax. RTC <READ | WRITE>"));
         }
         break;
      }

      case t_DHT:
      {
         switch (tokenBuffer[1])
         {
            case t_READ:
               Serial.print(F("Current Temperature: "));
               Serial.print(temp);
               Serial.print(F("C | "));
               Serial.print(convertToF(temp));
               Serial.println(F("F"));
               Serial.print(F("Current Humidity: "));
               Serial.print(humidity);
               Serial.println(F("%"));
               break;
            case t_PRINT:
               dumpLog(); 
               break;
            case t_EXTREME:
               Serial.print(F("Highest Temp: "));
               Serial.println(highestTemp);
               Serial.print(F("Lowest Temp: "));
               Serial.println(lowestTemp);
               break;
            default:
               Serial.println(F("ERROR: Invalid command syntax. DHT <READ | PRINT>"));
         }
         break;
      }

      case t_ADD:
         Serial.println(newInterval);
         newInterval = 0;
         break;
      
      case t_RGB:
         Serial.println(F("Set the RGB LED's Color. \"t\" to increment. \"r\" to decrement."));
         Serial.print(F("RED: "));
         Serial.print(r, DEC);
         getInput();
         r = parseInputToUnsignedNumber(0, 255);
         
         Serial.print(F("GRN: "));
         Serial.print(g, DEC);
         getInput();
         g = parseInputToUnsignedNumber(0, 255);

         Serial.println();

         Serial.print(F("BLU: "));
         Serial.print(b, DEC);
         getInput();
         b = parseInputToUnsignedNumber(0, 255);
         Serial.println();

         leds.setPixelColor(0, leds.Color(r, g, b));
         leds.show();

         Serial.print(F("RGB LED set to: "));
         Serial.print(r, DEC);
         Serial.print(",");
         Serial.print(g, DEC);
         Serial.print(",");
         Serial.println(b, DEC);
         break;
      
      case t_VERSION:
         Serial.print(F("Program: v"));
         Serial.println(versionNumber);
         break;

      case t_HELP:
         Serial.println(F("DH13 <READ | PRINT | EXTREME> .........: Reads current temp/humidity or prints log or gives highest/lowest temps"));
         Serial.println(F("ADD <NUM1> <NUM2> .....................: Adds two positive numbers together"));
         Serial.println(F("LED <GREEN | RED | ON | OFF | BLINK> ..: Sets the bicolor LED"));
         Serial.println(F("TIME <WRITE | READ> ...................: Allows you to read and write current time"));
         Serial.println(F("D13 <ON | OFF | BLINK> ................: Sets the onboard LED"));
         Serial.println(F("SET BLINK <0 - 65535> .................: Sets the blink rate in ms"));
         Serial.println(F("STATUS LEDS ...........................: Returns status of both LEDs"));
         Serial.println(F("VERSION ...............................: Returns the program's version number"));
         Serial.println(F("HELP ..................................: Returns the available commands"));
         break;

      default:
         Serial.println(F("ERROR: Invalid command syntax. Type HELP for list of commands."));
   }
   
   // Clear Token Buffer
   tokenBufferIndex = 0;
   tokenBuffer[0] = t_EOL;
}

void tokenize(void)
{
   unsigned int scanIndex = 0;
   
   while(inputBuffer[scanIndex] != '\0')
   {
      unsigned int startTokenIndex = scanIndex;
      unsigned int endTokenIndex = scanIndex;
      
      // Finds the last character of the current token.
      while(inputBuffer[endTokenIndex] != ' ' && inputBuffer[endTokenIndex] != '\0')
      {
         endTokenIndex++;
      }

      int tokenSize = endTokenIndex - startTokenIndex;
     
      // Checks if token is all numbers
      bool tokenIsANumber = true;
      for(unsigned int i = startTokenIndex; i < endTokenIndex; i++)
      {
         if (!isDigit(inputBuffer[i]))
         {
            tokenIsANumber = false;
            break;
         }
      }

      if (tokenIsANumber)
      {
         // Adds token to signify two bytes will follow
         tokenBuffer[tokenBufferIndex] = t_WORD;
         tokenBufferIndex++;

         // Replaces each character integer with its integer value
         for (unsigned int i = startTokenIndex; i < endTokenIndex; i++)
            inputBuffer[i] = charToInt(inputBuffer[i]);
         
         if (tokenSize > 6)
         {
            Serial.println(F("WARNING: Value entered is too large. Maximum value is 65,535 ms"));
            newInterval = 65535;
         }
         else
         {
            int previousValue = 0;
            for (int i = endTokenIndex - 1, j = 0; i >= startTokenIndex; i--, j++)
            {
               previousValue = newInterval;
               newInterval += (inputBuffer[i] * power(10, j));
               if (newInterval < previousValue)
               {
                  newInterval = 65535;
                  break;
               }
            }
         }
         tokenBufferIndex++;
      }
 
      // Search commandTable for tokens
      for (int i = 0; i < COMMAND_TABLE_SIZE; i++)
      {
            // If a match enter into tokenBuffer
            if ( (commandTable[i][0] == inputBuffer[startTokenIndex]) && (commandTable[i][1] == inputBuffer[startTokenIndex + 1]) 
              && (commandTable[i][2] == tokenSize))
         {
            if (tokenBufferIndex <  (TOKEN_BUFFER_SIZE - 1) )
            {
               tokenBuffer[tokenBufferIndex] = commandTable[i][3];
               tokenBufferIndex++;
               break;
            }
         }
      }
      scanIndex = endTokenIndex + 1;
   }

   if (inputBuffer[scanIndex] == '\0')
   {
      tokenBuffer[tokenBufferIndex] = t_EOL;
      tokenBufferIndex++;
   }

   // Clear inputBuffer
   inputBufferIndex = 0;
   inputBuffer[0] = '\0';

   Serial.println();
   executeTokens();
   Serial.println();
}


void printInputBuffer(void)
{
  Serial.print(F("\r                                                                                       \r"));
  Serial.print(">");
  for (int i = 0; i < inputBufferIndex; i++)
     Serial.write(inputBuffer[i]);
}

void serialInput(void)
{
   if (Serial.available() > 0)
   {
      char c = Serial.read();

      // Checks for whitespace in the front and discards it      
      if (inputBufferIndex == 0 && (c == ' ' || c == '\t' || c == '\v' || c == '\f' || c == '\r' || c == '\n'))
         return;
      
      if((c == 127 || c == 8) && inputBufferIndex != 0)
      {
         inputBufferIndex--;
         printInputBuffer();
         return;
      }

      // If character is uppercase, make it lowercase
      if (c >= 'A' && c <= 'Z')
         c = c + 32;

      // Adds zero to end of string and sends it to be tokenized
      if (c == 13)
      {
         inputBuffer[inputBufferIndex] = '\0';
         inputBufferIndex++;
         tokenize();
      }
      else if (inputBufferIndex == INPUT_BUFFER_SIZE - 1)
      {
         Serial.println(F("ERROR: Input Buffer is full. Press ENTER to commit current buffer."));
      }
      else
      {
         inputBuffer[inputBufferIndex] = c;
         inputBufferIndex++;
      }
      printInputBuffer();
   }

}



void setup()
{
   Serial.begin(9600);
   Serial.print(F("Starting serial input."));
   while(!Serial) {
      Serial.print(F(".")); // Null Statement to wait for serial to connect.
   }
   Serial.println(F("Done."));
   Serial.println(F("Ready to receive commands."));
   Serial.print(F(">"));


   d13LedActionTime = 0;
   biColorLedActionTime = 0;
   RGBLedActionTime = 0;


   Clock.begin();
   dht22.begin();

   highestTemp = lowestTemp = dht22.readTemperature();

   leds.begin();
   leds.setBrightness(64);
   pinMode(D13PIN, OUTPUT);
   pinMode(GREENPIN, OUTPUT);
   pinMode(REDPIN, OUTPUT);

 
}

void loop()
{
   MyDateTime = Clock.read();
   if (MyDateTime.Second % 5 == 0)
   {
      temp = dht22.readTemperature();
      humidity = dht22.readHumidity();

      if (temp > highestTemp)
         highestTemp = temp;
      if (temp < lowestTemp)
         lowestTemp = temp;

   }

   if (MyDateTime.Minute % 15 == 0)
   {
      // Create log to be entered
      Log loggedData;
      loggedData.humidity = humidity;
      loggedData.temp = temp;

      // Log it
      Clock.writeLog(loggedData);
   }   

   multitasker();
   serialInput();
}

void multitasker(void) 
{
   // Controls D13 LED
   if (readBitFromByte(ledFlags, D13ON))
   {
      if (millis() >= d13LedActionTime)
      {
         // Update time for Next Action
         d13LedActionTime = millis() + d13LedDelayInterval;
         // Rotate pattern
         d13LedPattern = leftRotate(d13LedPattern, 1);
         // Reads MSB from byte pattern and sets it to D13
         digitalWrite(D13PIN, readBitFromByte(d13LedPattern, 8) );
      }
   }
   else 
   {
      // Set to 0 to ensure it activates when power is on
      d13LedActionTime = 0;
   }

   // Controls biColor LED
   if (readBitFromByte(ledFlags, BICOLORON))
   {
      if (millis() >= biColorLedActionTime)
      {
         // Update time for Next Action
         biColorLedActionTime = millis() + biColorLedDelayInterval;
         // Rotate pattern by 2
         biColorLedPattern = leftRotate(biColorLedPattern, 2);

         // Reads MSB from byte pattern and sets it to RED Pin. Second MSB to GREEN Pin.
         digitalWrite(REDPIN, readBitFromByte(biColorLedPattern, 8) );
         digitalWrite(GREENPIN, readBitFromByte(biColorLedPattern, 7) );
      }
   }
   else 
   {
      // Set to 0 to ensure it activates when power is on
      biColorLedActionTime = 0;
   }

   // Controls RGB LED
   if (readBitFromByte(ledFlags, RGBON))
   {
      if (millis() >= RGBLedActionTime)
      {
         // Update time for Next Action
         RGBLedActionTime = millis() + RGBLedDelayInterval;
         // Rotate pattern
         RGBLedPattern = leftRotate(RGBLedPattern, 1);
         // Reads MSB from byte pattern and sets it to RGB
         if(readBitFromByte(RGBLedPattern, 8))
            leds.clear();
         else
         {
            leds.setPixelColor(0, leds.Color(r, g, b));
            leds.show();
         }
      }
   }
   else 
   {
      // Set to 0 to ensure it activates when power is on
      RGBLedActionTime = 0;
   }
}


/********************
 * Helper Functions *
 ********************/

// Checks if the bit in a byte is set. (1 = LSB, 8 = MSB)
bool readBitFromByte(byte byteField, int bit)
{
   if (bit > 0 && bit <= 8)
      return (byteField & (1 << (bit - 1) ) );
   else
      return 0;
}

// Celcius to Farenheit
float convertToF(float cel)
{
   float f = cel * 9.0 / 5.0;
   f += 32;
   return f;
}

// Left Rotate bitfield by n bits
byte leftRotate(byte bitfield, unsigned int n)
{
   return (bitfield << n) | (bitfield >> (8 - n));
}

// Gets input from User and adds it to Input Buffer
void getInput(void)
{
   inputBufferIndex = 0;
   inputBuffer[0] = '0';

   char c = Serial.read();
   if (Serial.available() > 0)
   {
      Serial.print("GET IN");
      c = Serial.read();

      // Checks for whitespace in the front and discards it      
      if (inputBufferIndex == 0 && (c == ' ' || c == '\t' || c == '\v' || c == '\f' || c == '\r' || c == '\n'))
         return;
      
      // Checks for backspace or delete entered
      if((c == 127 || c == 8) && inputBufferIndex != 0)
      {
         inputBufferIndex--;
         printInputBuffer();
         return;
      }

      // If character is uppercase, make it lowercase
      if (c >= 'A' && c <= 'Z')
         c = c + 32;

      // Adds zero to end of string
      if (c == 13)
      {
         inputBuffer[inputBufferIndex] = '\0';
         inputBufferIndex++;
         return;
      }
      else if (inputBufferIndex == INPUT_BUFFER_SIZE - 1)
      {
         Serial.println(F("ERROR: Input Buffer is full. Press ENTER to commit current buffer."));
      }
      else
      {
         inputBuffer[inputBufferIndex] = c;
         inputBufferIndex++;
      }
      printInputBuffer();
   }

}

bool isANumber(int bufferSize)
{
   for(int i = 0; i < bufferSize; i++)
   {
      if (isDigit(inputBuffer[i]) == false)
         return false;
   }
   return true;
}

unsigned int parseInputToUnsignedNumber(unsigned int min_size, unsigned int max_size)
{
   while(isANumber(inputBufferIndex) == false)
   {
      Serial.print(F("ERROR: Please enter a valid number: "));
      getInput();
   }

   unsigned int result = 0;

   for (int i = 0; inputBuffer[i] != '\0'; ++i)
      result = result * 10 + inputBuffer[i] - '0';

   if (result > max_size)
      result = max_size;
   if (result < min_size)
      result = min_size;
   return result;
}